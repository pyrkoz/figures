package Figure;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        MyFigure figura = new MyFigure(2,3, 5, 4);

        System.out.println("X = " + figura.getX());
        System.out.println("Y = " + figura.getY());
        System.out.println("Wysokość = " + figura.getHeight());
        System.out.println("Długość = " + figura.getLength());

        figura.moveFigure(8,8);
        figura.changeFigureSize(10, 99);
        figura.rotateFigure(90);
        figura.rotateFigure(999);

        System.out.println("Czy parametry figury się zmieniły?");
        System.out.println("X = " + figura.getX());
        System.out.println("Y = " + figura.getY());
        System.out.println("Wysokość = " + figura.getHeight());
        System.out.println("Długość = " + figura.getLength());
        System.out.println("Nie, gdyż raz wywołany obiekt niezmienny nigdy nie zmieni swojej wartości.");
    }
}
