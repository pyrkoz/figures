package Figure;

final public class MyFigure {
    final private int x;
    final private int y;
    final private int length;
    final private int height;

    public MyFigure(int x, int y, int height, int length) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.length = length;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getLength() {
        return length;
    }

    public int getHeight() { return height; }

    public MyFigure moveFigure(int x, int y) {
        return new MyFigure(x, y, height, length);
    }

    public MyFigure changeFigureSize(int height, int length) {
        return new MyFigure(x, y, height, length);
    }

    public MyFigure rotateFigure(int angle) {
        if(angle == 90 || angle == 270)
            return new MyFigure(x, y, length, height);
        else if(angle > 360 || angle < 0) {
            System.out.println("Błędny kąt!");
            return new MyFigure(x, y, height, length);
        }
        else
            return new MyFigure(x, y, height, length);
    }
}
